<?php

declare(strict_types=1);

namespace Drupal\Tests\username_enumeration_prevention\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests user password reset form.
 *
 * @group username_enumeration_prevention
 */
class UserPassTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'username_enumeration_prevention',
    'username_enumeration_prevention_password_test',
  ];

  /**
   * Asserts messages on user password form.
   */
  public function testUserPassForm(): void {
    $this->drupalGet(Url::fromRoute('user.pass'));
    $edit = ['name' => $this->randomMachineName()];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('You shall not pass!');
    $this->assertSession()->pageTextContains('You shall not pass!');

  }

}
